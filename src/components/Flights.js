import React from 'react';
import Passengers from './Passengers';

class Flights extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showDetails: false
		};
		


		this.toggleDetails = this.toggleDetails.bind(this);
		this.flightDetails 	= this.flightDetails.bind(this)
	}

	toggleDetails() {
		this.setState({showDetails: !this.state.showDetails});
	}
	

	

	flightDetails() {
		const {arrival, arrivalTime, departure, departureTime,duration,flight,id,passengers} = this.props.flight;
		const passenger = <Passengers key={this.props.index}  passengers={passengers}/> 
	
		return (
			<div>
				{this.state.showDetails ?
					<ul>
						<li>Arrival: {arrival}</li>
						<li>arrivalTime: {arrivalTime}</li>
						<li>departure: {departure}</li>
						<li>departureTime: {departureTime}</li>
						<li>duration: {duration}</li>
						<li>flight: {flight}</li>
						<li>id: {id}</li>
						<li >{passenger}</li>
					</ul> : null
				}
			</div>
		);
	}
	
	render () {
		const { flight } = this.props;
		
		console.log("Flight",flight.flight)
		const flightDetails = this.flightDetails(); 



		return (
			<li onClick={this.toggleDetails}>
				{flight.flight}
				{flightDetails}

			</li>
		);
	}
}

export default Flights; 
