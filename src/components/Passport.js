import React from 'react';

class Passport extends React.Component {
	

	render() {
			const passport = this.props.passport.map((p)=>{
				return(
					<div>
						<li>{p.type}</li>
						<li>{p.expiryDate}</li>
					</div>
				);
			});

			
		return (
			<ul  >
				{passport}
			</ul>

		);
	}
}

export default Passport;