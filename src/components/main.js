import React from 'react';
import axios from 'axios';
import SearchBar from './SearchBar';

import Flights from './Flights';

class Main extends React.Component {
	constructor (){
		super();
		this.state = {
			flightList: [],
			cmpList: [{name: 'list', isActive: true}, {name: 'edit', isActive: false}],
			filterText: ''
		};

		

		this.renderActiveCmp = this.renderActiveCmp.bind(this);
		//this.showFlightDetails = this.showFlightDetails.bind(this); 
		this.handleFilterInput = this.handleFilterInput.bind(this);
	}

	handleFilterInput(filterText){
		this.setState({filterText:filterText})

	}

	componentDidMount() {
    axios.get('http://localhost:8080/test.json')
      .then(response => this.setState({ flightList: response.data }) )
	    .catch(function (error) {
	      console.log(error);
	    });



  }

	renderActiveCmp() {
		let flights;
		
		
		flights = this.state.flightList.map((flight,index) => {
			console.log("flight",flight)
			if(this.state.filterText ===""){
				return (
				<Flights key={index} index={index} flight={flight} filterText={this.state.filterText}/>
				)
			}else if(this.state.filterText === flight.flight){
				return (
				<Flights key={index} index={index} flight={flight} filterText={this.state.filterText}/>
				)
			}
		});
			
		

			

		return (
			<div>
				<SearchBar 
					filterText={this.state.filterText}
					handleFilterInput={this.handleFilterInput}/>
				
				<ul>{flights}</ul>
			</div>
		);
	}

	render() {

			



		return this.renderActiveCmp()
	}

}
export default Main;

