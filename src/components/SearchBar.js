import React from 'react';

class SearchBar extends React.Component {

	handleChange(e){
		this.props.handleFilterInput(e.target.value)
	}

	render(){
		this.handleChange = this.handleChange.bind(this);
		return (
			<form>
        <input 
        	type="text" placeholder="Search..." 
        	value={this.props.filterText}
        	onChange={this.handleChange}  />
      </form>
		);
	}
}

export default SearchBar;
