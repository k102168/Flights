import React from 'react';
import Passport from './Passport';

class Passengers extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showPassenger: false,
			showPassport: true
		};
		this.passengerName 	= this.passengerName.bind(this)
		this.toggleDetails = this.toggleDetails.bind(this);
		this.togglePassport = this.togglePassport.bind(this);
	}
	toggleDetails(e) {
		e.stopPropagation();

		this.setState({
			showPassenger: !this.state.showPassenger,
			showPassport: !this.state.showPassport
		});
	}

	togglePassport(e) {
		e.stopPropagation();

		this.setState({
			showPassport: !this.state.showPassport
		});
	}
	
	
	passengerName() {
		const passenger = this.props.passengers.map((p,index)=>{
			return(
				<div>
					<li>{p.name}</li>
					<li onClick={this.togglePassport}>Passport: {this.state.showPassport? <Passport passport={p.passport}/>: null}</li>
				</div>
			)
		}) 
		return passenger;
	}

	render(){
		const passengerName = this.passengerName();

		return (
			<div onClick={this.toggleDetails}>
				Passengers: {this.props.passengers.length}
				<ul>{this.state.showPassenger ? passengerName: null}</ul>
			</div>
		);
	}
}

export default Passengers;