import React, { Component } from 'react';

import Header from './components/header';
import Main from './components/main';

import './App.css';

class App extends Component {




  render() {

    let test=[
  {
    "id": 1,
    "flight": "Airblue",
    "duration": "2hours",
    "departure": "karachi",
    "arrival": "Lahore",
    "departureTime": "2017 - 09 - 27 T18: 30: 49 - 0300",
    "arrivalTime": "2017 - 09 - 27 T20: 30: 49 - 0300",
    "passengers": [{
        "name": "john",
        "age": "20",
        "nationality": "USA",
        "passport": [{
          "type": "Oridinary",
          "expiryDate": "2019 - 09 - 27 T20: 30: 49 - 0300"
        }, {
          "type": "Official",
          "expiryDate": "2016 - 09 - 27 T20: 30: 49 - 0300"
        }]
      },
      {
        "name": "Walter",
        "age": "20",
        "nationality": "German",
        "passport": [{
          "type": "Oridinary",
          "expiryDate": "2019 - 09 - 27 T20: 30: 49 - 0300"
        }, {
          "type": "Official",
          "expiryDate": "2016 - 09 - 27 T20: 30: 49 - 0300"
        }]
      }
    ]

  },
  {
    "id": 2,
    "flight": "Pakistan International Airline",
    "duration": "4hours",
    "departure": "karachi",
    "arrival": "abbottabad ",
    "departureTime": "2017 - 09 - 27 T18: 30: 49 - 0300",
    "arrivalTime": "2017 - 09 - 27 T22: 30: 49 - 0300",
    "passengers": [{
        "name": "Abdullah",
        "age": "17",
        "nationality": "Pakistan",
        "passport": [{
            "type": "Oridinary",
            "expiryDate": "2019 - 09 - 27 T20: 30: 49 - 0300"
          },
          {
            "type": "Official",
            "expiryDate": "2016 - 09 - 27 T20: 30: 49 - 0300"
          }
        ]
      },
      {
        "name": "john",
        "age": "20",
        "nationality": "USA",
        "passport": [{
            "type": "Oridinary",
            "expiryDate": "2019 - 09 - 27 T20: 30: 49 - 0300"
          },
          {
            "type": "Official",
            "expiryDate": "2016 - 09 - 27 T20: 30: 49 - 0300"
          }
        ]
      }
    ]
  },
  {
    "id": 3,
    "flight": "Shaheen Air",
    "duration": "8hours",
    "departure": "karachi",
    "arrival": "gilgit baltistan",
    "departureTime": "2017 - 09 - 27 T18: 30: 49 - 0300",
    "arrivalTime": "2017 - 09 - 28 T01: 30: 49 - 0300",
    "passengers": [{
        "name": "anil",
        "age": "25",
        "nationality": "indian",
        "passport": [{
            "type": "Oridinary",
            "expiryDate": "2019 - 09 - 27 T20: 30: 49 - 0300"
          },
          {
            "type": "Official",
            "expiryDate": "2016 - 09 - 27 T20: 30: 49 - 0300"
          }
        ]
      },
      {
        "name": "Walter",
        "age": "20",
        "nationality": "German",
        "passport": [{
            "type": "Oridinary",
            "expiryDate": "2019 - 09 - 27 T20: 30: 49 - 0300"
          },
          {
            "type": "Official",
            "expiryDate": "2016 - 09 - 27 T20: 30: 49 - 0300"
          }
        ]
      }
    ]
  }

] 

    const filter = test.reduce((t)=>{
        return (t.flight === "Airbluea"?t:test)
    })
    console.log(filter)

    return (
      <div>
        <Header/>
        <Main/>
      </div>
      
    );
  }
}

export default App;
